#!/bin/bash

unsnap()
{
	while [ -n "$(snap list | cut -d ' ' -f 1)" ]; do
		for i in $(snap list | cut -d ' ' -f 1); do
			snap remove $i;
		done
	done

	apt -y purge snapd
	add-apt-repository -y ppa:mozillateam/ppa
	apt -y install firefox-esr
	rm -rf /var/lib/snapd
}

fix_dpkg()
{
	apt --fix-broken -y install
}

prereq()
{
	PACKAGE="$1"
	DEPENDENCIES="$WORKDIR/$ROOM/scripts/$PACKAGE.txt"
	for i in $(cat "$DEPENDENCIES"); do
		install_package "$i" | tee "$LOGDIR/$PACKAGE.txt"
	done
}

install()
{
	PACKAGE="$WORKDIR/$ROOM/scripts/$1"
		dpkg --install "$PACKAGE.deb"
}

handle()
{
	prereq $1
	install $1
}

packet-tracer()
{
	PACKAGE="packet-tracer"

	handle "$PACKAGE"

	unset PACKAGE
	unset FOLDER
}

docker()
{
	PACKAGE="docker"
	FOLDER="$WORKDIR/$ROOM/scripts/$PACKAGE"
	VERSION='v4.11.0'
	LINK="https://desktop.docker.com/linux/main/amd64/docker-desktop-4.11.0-amd64.deb?utm_source=docker&utm_medium=webreferral&utm_campaign=docs-driven-download-linux-amd64"

	mkdir -p /etc/apt/keyrings
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
	echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" > /etc/apt/sources.list.d/docker.list
	apt update

	wget "$LINK" -O "$FOLDER.deb"

	handle "$PACKAGE"
	usermod -aG docker "$LUSER"
}

atom()
{
	PACKAGE="atom"
	FOLDER="$WORKDIR/$ROOM/scripts/$PACKAGE"
	VERSION='v1.60.0'
	LINK="https://github.com/atom/atom/releases/download/$VERSION/atom-amd64.deb"

	wget "$LINK" -O "$FOLDER.deb"

	handle "$PACKAGE"

	unset PACKAGE
	unset FOLDER
	unset VERSION
	unset LINK
}

virtual-box()
{
	PACKAGE="virtual-box"
	FOLDER="$WORKDIR/$ROOM/scripts/$PACKAGE"
	VERSION='6.1_6.1.26-145957'
	LINK="https://download.virtualbox.org/virtualbox/6.1.26/virtualbox-$VERSION~Ubuntu~eoan_amd64.deb"

	wget "$LINK" -O "$FOLDER.deb"

	handle "$PACKAGE"

	unset PACKAGE
	unset FOLDER
	unset VERSION
	unset LINK
}

arduino()
{
	PACKAGE="arduino"
	FOLDER="$WORKDIR/$ROOM/scripts/$PACKAGE"
	VERSION='1.8.15'
	LINK="https://downloads.arduino.cc/arduino-$VERSION-linux64.tar.xz"

	wget "$LINK" -O "$FOLDER.tar.xz"
	tar -xf "$FOLDER.tar.xz"
	mv "$PACKAGE-$VERSION" /usr/share
	cd "/usr/share/$PACKAGE-$VERSION"
	./install.sh | tee "$LOGDIR/PACKAGE.txt"
	cd -
	adduser "$LUSER" dialout
	rm "$FOLDER.tar.gz"

	unset PACKAGE
	unset FOLDER
	unset VERSION
	unset LINK
}

python()
{
	PACKAGE="Python"
	FOLDER="$WORKDIR/$ROOM/scripts/$PACKAGE"
	VERSION='3.9.6'
	LINK="https://www.python.org/ftp/python/$VERSION/Python-$VERSION.tar.xz"

	wget "$LINK" -O "$FOLDER.deb"
	wget "$LINK" -O "$FOLDER.tar.xz"
	tar -xf "$FOLDER.tar.xz"
	cd "$PACKAGE-$VERSION"
	./configure | tee "$LOGDIR/$PACKAGE.txt"
	make -j"$(expr $(nproc) \+ 1)" | tee "$LOGDIR/$PACKAGE.txt"
	make -j"$(expr $(nproc) \+ 1)" install | tee "$LOGDIR/$PACKAGE.txt"
	cd -

	unset PACKAGE
	unset FOLDER
	unset VERSION
	unset LINK
}

minicom()
{
	echo -e "pu port\t/dev/ttyUSB0\npu baudrate\t9600\npu bits\t8\npu parity\tN\npu stopbits\t1\npu rtscts\tNo\n" > /etc/minicom/minirc.dfl
}

wireshark()
{
	groupadd wireshark
	usermod -aG wireshark "$LUSER"
	chgrp wireshark /usr/bin/dumpcap
	chmod 754 /usr/bin/dumpcap
	setcap 'CAP_NET_RAW+eip CAP_NET_ADMIN+eip' /usr/bin/dumpcap
	usermod -aG dialout aluno
}

chrome()
{

	PACKAGE="Chrome"
	FOLDER="$WORKDIR/$ROOM/scripts/$PACKAGE"
	VERSION='current'
	LINK="https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb"

	wget "$LINK" -O "$FOLDER.deb"

	handle "$PACKAGE"

	unset PACKAGE
	unset FOLDER
	unset VERSION
	unset LINK

}

precomp()
{
	unsnap
	python
	virtual-box
	packet-tracer
	wireshark
	docker
	chrome
	minicom
}

