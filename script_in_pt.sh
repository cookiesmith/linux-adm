#!/bin/bash

look_in()
{
	#Procura por um mac específico, dentro do arquivo dado
	mac_list=($(cat $1))	#Lemos a lista de macs, para um vetor
	for i in ${mac_list[@]}; do
		if [ "$mac" = "$(echo $i | cut -b 4-20)" ]; then #Retirar o mac e comparar
			echo "$(echo $i | cut -b 1-2)" #Se encontrou, retorna o id do pc para a função pai
			exit
		fi
	done
	echo "0" #Não existe id 0, então significa que o mac não foi encontrado
	exit
}

id_mac()
{
	#É um wrapper. Para cada sala, existe uma lista de macs para ser verificada
	for mac in ${macs[@]}; do	#O computador pode ter mais de um mac a ser verificado
		for dir in $(ls $WORKDIR); do	#Lista as salas
			if [ -a $WORKDIR/$dir/data/macs ]; then	#Tomar a lista de macs de uma sala específica
				number="$(look_in $WORKDIR/$dir/data/macs)"
				if [ "$number" != "0" ]; then
					echo "$dir$number"
					exit
				fi
			fi
		done
	done
	echo "$DEFAULT_DIR""$( expr $(tail -n 1 $WORKDIR/$DEFAULT_DIR/data/macs | cut -b 1-2) + 01 )" #atualiza ID baseado em pcs desconhecidos
	exit
}

room_of()
{
	echo "$( echo $1 | cut -b 1-3 )"	#Extrai o número da sala, dado um PCID
}

id_of()
{
	echo "$( echo $1 | cut -b 4-5 )"	#Extrai o id do computador, dado um PCID
}

room_exists()
{
	#Verifica a existência de uma sala
	if [ -d $WORKDIR/"$(room_of $1)" ]; then
		echo "0"
	else
		echo "1"
	fi
	exit
}

validate_pcid()
{
	#Valida o PCID. DEVE ter 5 dígitos, e a sala deve existir. Se não válido, retorna o PCID padrão
	validate=$1

	if [ ${#validate} -eq 5 ]; then
		if [ "$(room_exists $( room_of $validate ) )" = "0" ];then
			echo $validate
		else
			echo "$DEFAULT_DIR""$($id_of $validate)"
		fi
	else
	echo "$DEFAULT_DIR""$( expr $(tail -n 1 $WORKDIR/$DEFAULT_DIR/data/macs | cut -b 1-2) + 01 )" #Atualiza ID baseado em pcs desconhecidos
	fi
}

add_mac()
{
	#Salva os macs da máquina no arquivo especificado pelo PCID
	if ! [ -d "$WORKDIR/$(room_of $1)" ] ; then
		PCID="$DEFAULT_DIR$( id_of $1 )"
	else
		PCID="$1"
	fi

	ROOM="$(room_of $PCID)"

	for mac in ${macs[@]}; do	#For every mac for this computer, we save each with one numeral
		echo "$( id_of $PCID )"'-'$mac >> $WORKDIR/$ROOM/data/macs
		PCID="$(validate_pcid "$(( 10#$(expr $PCID) + 1 ))")"
	done
}

change_id()
{
#É um wrapper. Após remover um MAC escpecificado de um arquivo, adiciona o mac à outro.
	NEW_PCID="$(validate_pcid $1)"
	ROOM=$(room_of $PCID)
	NUMBER=$(id_of $PCID)
	if [ -a $WORKDIR/$ROOM/data/macs ]; then
		index=0
		for i in $(cat $WORKDIR/$ROOM/data/macs); do
			lista[index]=$i
			index=$(( $index + 1 ))
		done

		for mac in ${macs[@]}; do
			index=0
			while [ $index -lt ${#lista[@]} ]; do
				if [ "$mac" = "$( echo ${lista[$index]} | cut -b 4-20)" ]; then
					lista=( ${lista[@]:0:$index} ${lista[@]:$(( $index + 1 ))} )
					index=$(( $index - 1 ))
				fi
				index=$(( $index + 1 ))
			done
		done

		echo ${lista[0]} > $WORKDIR/$ROOM/data/macs
		for i in ${lista[@]:2}; do
			echo $i >> $WORKDIR/$ROOM/data/macs
		done

	fi
	add_mac $NEW_PCID
	echo $NEW_PCID
	exit
}

wait_input()
{
	#Espera por uma entrada, ou segue o procedimento
	if [ $1 -ge 0 ]; then
		mp=$(( $TIME*$1 ))
	else
		mp=$TIME
	fi

	read -n 1 -t $mp
	if [ $? == 0 ]; then
		echo "1"
		exit
	fi
	echo "0"
	exit
}

verify_int()
{
	if ! [ -a tmp/last_good.tar ] && ! [ -a tmp/hash ]; then
		echo "1"
		exit
	fi

	tar cfv tmp/verify.tar spec > tmp/verify.log	#Gera um novo tar para verificar o diretório
	compromissed=$( sha512sum --quiet -c tmp/hash )	#Se sha512sum retornar algo, o diretório foi alterado

	if [ -z "$compromissed" ]; then	#Se a variável foi setada, o arquivo está comprometido
		mv tmp/verify.tar tmp/last_good.tar	#Se não estiver comprometido, atualize o ultimo bom TAR e o LOG
		mv tmp/verify.log tmp/last_good.log
		echo "0"
	else
		echo "1"
	fi
	exit
}

update_hashs()
{
#Força o hash a ser atualizado, bem como o arquivo de backup
	tar cfv tmp/last_good.tar spec/ > tmp/last_good.log	#Gera um novo tar do diretório
	echo $(sha512sum tmp/last_good.tar) > tmp/hash	#Gera e salva o novo hash
}

uptodate()
{
	#Atualiza o computador, duh!
	rm -f /var/lib/dpkg/lock* #Removendo arquivo de trava do APT, pro caso de o processo ter sido morto, sem remover o lixo
	rm -f /var/lib/apt/lists/lock

	apt update
	apt -y full-upgrade
	apt -y autoremove
}


build_lists()
{
	#Contrução das listas de instalação e remoção

	echo "Construindo lista de aplicativos"

	cat $WORKDIR/$DEFAULT_DIR/$INSTALL > /tmp/prog_tmp #Salva a lista geral de aplicativos

	if [ "$ROOM" != "$DEFAULT_DIR"] && [ -a $WORKDIR/$ROOM/$INSTALL ]; then	#Se tiver uma lista específica da sala, concatene
		cat $WORKDIR/$ROOM/$INSTALL >> /tmp/prog_tmp
	fi
	cat $WORKDIR/$DEFAULT_DIR/$REMOVE > /$REMOVE #Ler a lista geral de remoções

	if [ "$ROOM" != "$DEFAULT_DIR" ] && [ -a $WORKDIR/$ROOM/$REMOVE ]; then #Procurar por listas específicas também
		cat $WORKDIR/$ROOM/$REMOVE >> /$REMOVE
	fi
	cat /tmp/prog_tmp | grep -v '$(cat /$REMOVE)' > /$INSTALL #Remover itens a serem removidos da lista de instalação

	echo "Desisntalando aplicativos não necessários"
	for i in $(cat /$REMOVE); do
		apt -y purge $i
	done

	echo "Instalando aplicativos necessários"
	for i in $(cat /$INSTALL); do
		apt -y install $i
	done

	rm /$INSTALL #Limpar a sujeira
	rm /$REMOVE
	rm /tmp/prog_tmp
}

loopback_conf()
{
	#Instalaçao do script de loopback, que reseta o usuário ao reiniciar a máquina
	echo "Time tavel machine time (loopback script)..."

	cp $WORKDIR/$DEFAULT_DIR/$SCRIPT /$SCRIPT

	if [ "$ROOM" != "$DEFAULT_DIR" ] && [ -a $WORKDIR/$ROOM/$SCRIPT ]; then	#Se houver detalhes na sala específica, concatene
		cat $WORKDIR/$ROOM/$SCRIPT >> /$SCRIPT
	fi

	if [ "$ROOM" != "$DEFAULT_DIR" ] && [ -a $WORKDIR/$ROOM/$SERVICE ]; then	#Mesma ideia para o serviço
		cp $WORKDIR/$ROOM/$SERVICE /$SERVICE
	else
		cp $WORKDIR/$DEFAULT_DIR/$SERVICE /$SERVICE
	fi

	chmod 450 /$SCRIPT	#Alterando permissões para evitar acessos indevidos
	chmod 440 /$SERVICE

	systemctl disable loopback.service	#Disabilitar o antigo daemon, e habilitar o novo
	systemctl enable loopback.service
}

hostname_config()
{
		if [ $( expr $PCID ) -lt 20000 ]; then	#Se o PCID não é o padrão, define o hostname da máquina como sendo esse.
			hostnamectl set-hostname $PCID
		fi
}

post_clear()
{
	#Remoção dos aplicativos instalados por suporte
	cat $WORKDIR/$DEFAULT_DIR/prog/prog_aft > /tmp/prog_aft
	if [ "$ROOM" != "$DEFAULT_DIR" ] && [ -a $WORKDIR/$ROOM/prog/prog_aft ]; then
		cat $WORKDIR/$ROOM/prog/prog_aft >> /tmp/prog_aft
	fi

	for i in $(cat /tmp/prog_aft); do
		apt purge -y $i
	done

	apt -y autoremove
	apt -y clean
}

grubify()
{
	#Personalização dos arquivos de configuração do GRUB

	if [ "$ROOM" != "$DEFAULT_DIR" ] && [ -a $WORKDIR/$ROOM/$GRUB ]; then
		cp $WORKDIR/$ROOM/$GRUB /$GRUB
	else
		cp $WORKDIR/$DEFAULT_DIR/$GRUB /$GRUB
	fi
	update-grub
}

package_config()
{
	#Instalando pacotes pré-compilados

	if [ -a "$WORKDIR/$ROOM/scripts/prec_ins.sh" ]; then
		bash $WORKDIR/$ROOM/scripts/prec_ins.sh $WORKDIR
	else
		bash $WORKDIR/$DEFAULT_DIR/scripts/prec_ins.sh $WORKDIR
	fi

}

finish()
{
	#Roda scripts específicos da sala, e chama a função de remoção dos aplicativos suporte

	post_clear

	echo "After job's for this room, if any (specifc room scripts)"
	if [ "$ROOM" != "$DEFAULT_DIR" ] && [ -a $WORKDIR/$ROOM/scripts/after.sh ]; then
		bash $WORKDIR/$ROOM/scripts/after.sh
	fi
	uptodate
}

find_impostors()
{
#Busca por usuários não padrão no computador, e deleta
	users=$(compgen -u) #Listar os nomes de usuário
	for user in $users; do
		useruid=$(id -u $user) #Conseguir o uid de cada usuário
		if [ $useruid -gt 999 ] && [ $useruid -lt 65534 ]; then #Significa que não é usuário do sistema/padrão
			delete=$(cat $WORKDIR/$ROOM/default_users | grep $user) #Comparar com a lista de usuários padrão
			if [ -z $delete ]; then
				deluser --remove-all-files $user
			fi
			unset delete
		fi
	done
}

user_config()
{
	#Adiciona o usuário e remove algumas permissões padrão
	echo "Você pode passar (adicionando usuários)"

	find_impostors

	useradd -m -d /home/$LUSER -s /bin/bash $LUSER
	useradd -m -r -s /bin/bash $AUSER

	echo "Só para garantir, vou resetar as senhas ($AUSER, $LUSER e root)"

	echo "$(cat /$SHADOW | grep -v $AUSER | grep -v $LUSER)" >> /tmp/shadow

	if [ -z "$AUSER_P" ]; then
		passwd -d $AUSER
	else
		echo "$AUSER:$AUSER_P:0:0:99999:7:::" >> /tmp/shadow
	fi

	if [ -z "$LUSER_P" ]; then
		passwd -d $LUSER
	else
		echo "$LUSER:$LUSER_P:0:0:99999:7:::" >> /tmp/shadow
	fi

	mv /tmp/shadow /$SHADOW
	chmod 640 /$SHADOW

	cat /$SUDOERS | grep -v %sudo | grep -v %admin > /tmp/sudoers
	cat $WORKDIR/$DEFAULT_DIR/$SUDOERS >> /tmp/sudoers

	if [ "$ROOM" != "$DEFAULT_DIR" ] && [ -a $WORKDIR/$ROOM/$SUDOERS ]; then
		cat $WORKDIR/$ROOM/$SUDOERS >> /tmp/sudoers
	fi

	mv /tmp/sudoers /$SUDOERS

	echo "E você não... (removendo permissões desnecessárias e grupos)"
	groupdel -f sudo
	groupdel -f admin
}

goodbye()
{
#GOOD-FCKN-BYE
	if [ "$(wait_input)" = "0" ]; then
		help
		read op
		menu ${op[@]}
		exit
	fi

	echo "Xau :)"
	shutdown -Ph now

}

hello()
{
	echo "#######################################################"
	echo "#     		   Olá script!			    #"
	echo "#     Script de configuração, por Allan Ribeiro       #"
	echo "#    email para:<allanribeiro@alunos.utfpr.edu.br>    #"
	echo "#######################################################"
}

help()
{
	echo "#######################################################"
	echo "#	  PCID atual:$PCID - Para trocar, use a função 3    #"
	echo "#		 Escolha um número para uma função	    #"
	echo "# 1) Verificar programa	  2) Atualizar hashes	    #"
	echo "# 3) Escolher PCID	  4) Troca/add MAC	    #"
	echo "# 5) Config. usuários	  6) Atualizar		    #"
	echo "#	7) Programas padrão (apt)  8) Programas padrão(prec)#"
	echo "#	9) Script Loopback	  10) Config. Hostname	    #"
	echo "#	11) grubify		  12) Script After-Jobs	    #"
	echo "#	13) Diga olá script!	  14) Sair		    #"
	echo "#######################################################"
}

menu()
{
	source $WORKDIR/$DEFAULT_DIR/variables
	if [ "$ROOM" != "$DEFAULT_DIR" ] && [ -a $WORKDIR/$ROOM/variables ]; then
		source $WORKDIR/$ROOM/variables
	fi

	for parameter in $@; do
		case $parameter in
			1 )
				if [ $(verify_int) -eq 0 ]; then
					echo "Algum arquivo foi modificado. Verifique \"compromissed\" para mais detalhes."
				else
					echo "Não foram encontradas alterações."
				fi
				;;
			2 )
				update_hashs
				;;
			3 )
				echo "Digite o PCID no formato xyzab (ex. 00120 para sala 001, máquina 20)"
				read NEW_PCID
				if ! [ -d $WORKDIR/"$(room_of $PCID)" ]; then
					NEW_PCID="$DEFAULT_DIR""$(id_of $PCID)"
				fi
				ROOM=$(room_of $NEW_PCID)
				source $WORKDIR/$ROOM/conf/variables
				;;
			4 )

				change_id
				;;
			5 )
				user_config
				;;
			6 )
				uptodate
				;;
			7 )
				build_lists
				;;
			8 )
				package_config
				;;
			9 )
				loopback_config
				;;
			10 )
				hostname_config
				;;
			11 )
				grubify
				;;
			12 )
				finish
				;;
			13 )
				hello
				;;
			14 )
				exit
				;;
			* )
				echo "Parâmetro inválido"
				help
				exit
		esac
	done
}

default_procedure()
{
	source $WORKDIR/$ROOM/conf/variables
	if [ "$ROOM" != "$DEFAULT_DIR" ] && [ -a $WORKDIR/$ROOM/variables ]; then
		source $WORKDIR/$ROOM/variables
	fi

	#Configuração de usuários

	echo "Configurando usuários"
	user_config

	#Atualizando

	echo "Atualizando o sistema"
	uptodate

	#Construição das listas de instalação e remoção. além de remoção e instalação destes aplicativos

	echo "Construindo listas de aplicativos, instalando os padrões e removendo os não essenciais"
	build_lists

	#Instalação manual dos pré-compilados

	echo "Configuração dos pré-compilados"
	package_config

	#Instalação do loopback

	echo "Instalação da máquina do tempo (script loopback)..."
	loopback_conf

	#Configuração do hostname

	echo -e "Mudando hostname da \"maquina\""
	hostname_config

	#reconfiguração do GRUB

	echo "Reconfigurando GRUB"
	grubify

	#Finalizando

	echo "Última verificação de pacotes e remoção dos aplicativos de suporte"
	finish

	echo "Malfeito feito! Desligando em $TIME segundos. Pressione qualquer tecla para cancelar"
	goodbye
}


main()
{
	if [ "$(whoami)" = "root" ]; then #Verificar a permissão do usuário em rodar este script
		echo "Verificando a integridade do programa"
		if [ $(verify_int) != 0 ]; then
			echo "Algum arquivo foi modificado. Verifique \"compromissed\" para mais detalhes."
			exit
		else
			echo "Não foram encontradas alterações."
		fi

		WORKDIR="$(pwd)/spec"	#Caminho para pasta das salas
		DEFAULT_DIR="200"	#Sala padrão
		TIME=5	#Tempo de delay padrão

		hello	#Diga olá, script
		nmcli n connectivity check	#Se não estiver logado na rede, force a tela de login aparecer
		sleep $TIME

		macs=($(lshw | grep -A 13 network | grep serial | cut -b 25-41))	#Extrair os macs da máquina
		PCID="$(id_mac)"	#Tentar identificar o mac
		ROOM="$(room_of $PCID)"	#Salvar o número da sala para uso das funções

		if ! [ $# -gt 0 ]; then #Se algum parametro foi dado, chame o menu. Caso contrário, rode o procedimento padrão
			echo "O PCID é $PCID. Estás certo disto? Pressione alguma tecla caso não"	#Caso o usuário queira alterar o PCID
			if [ "$(wait_input)" = "1" ]; then
				echo "O formato do PCID é SALAMAQUINA, como 00902 para sala 009 e máquina 02"
				echo "Caso seja inválido, o PCID toma o padrão $DEFAULT_ROOM""01"", E o mac será salvo lá com a instalação padrão"
				echo "Digite o PCID"
				read NEW_PCID
				PCID=$(change_id $NEW_PCID)
				ROOM="$(room_of $PCID)"	#Atualização do número da sala
			elif [ $( expr $PCID ) -gt 19999 ]; then	#O mac pode não ter mudado, mas pode não ter identidade. Vamos adicionar
				add_mac $PCID
			fi

			default_procedure

		else #Chamada do menu
			menu "$@"
		fi
	else
		echo "Obrigado por evitar permissões administrativas. Mas neste script é necessário (tente com sudo)"
	fi
}

main
