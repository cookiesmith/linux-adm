# sysconfig.sh - DAINF, UTFPR

## NAME

sysconfig.sh - Configure the operational system, according to V block
patterns

## SYNOPSIS

sysconfig.sh [_options_]

## DESCRIPTION

His premisse is to facilitate manangement of V block machines. We have
plenty computers, so the phisical maintenance of each computer,
periodically, is impractible. This script automates the configurations,
requiring only supervision. It will, yet, be written a script to ensure
this script success.

The script, by itself, will not require human interaction other than to
start it. No specific option is required.

Common problems:

If a licensed package (such as Cisco PacketTracer, and VirtualBox) is
installed, it will require a license to be accepted. It will require user
intervention.

The GNU/Linux distribution might be any derivative from Debain. Debian is
recommended. But be aware: on Debian, fresh installs needs to have
"/etc/apt/sources.list" check, as it might contain disk dependant
repositories active.

Configurations such as partitioning table are irrelevant, but a few
recommendations are ought to be made:

Separate /boot (1GB). Prevents boot failure when disks are full.

Separate /var (>10GB). Some Dell computers have problems with TPM handling
through linux kernel, and returns lot of errors, which grows logs. Find a
solution, or clean logs from time to time.

Separate /home (<20GB). You will be impressed by the capacity of students
in filling up the disk with garbage.


## TASKS

The script is actualy a scripted version of common tasks an system admin
has to do:

	Configure users

	update the computer

	idenfity accordingly

	install required apps (some of them compiled, some downloaded, but
most from repositories)

	Configure SSH

Each of those tasks were separated in functions, and may be called
separatly, but the most common case is to run them all.

## OPTIONS

NOTE: All options marked by * are **customized**

	--info

		Returns info about the computer identification

	--test-hash

		Check whether the program has been altered

	--update-hash

		Update changes made in the program

	--change-id

		Change the PC identification for this session

	--change-id-permanent

		Change the PC identification for every run

	--ssh-config *

		Install SSH config files and keys

	--package-conf *

		Build programs list and install them

	--incognito-conf *

		Install the incognito service

	--hostname-conf *

		Change the hostname

	--grubify *

		Install grub config files, and update it

	--find-impostors

		Identify and remove non-default users

	--user-conf

		Install default users config files

	--help

		Show this message

## FUNCTIONS

The functions will be explained in evocation order, as in the
**default_procedure**, which follows without user intervention (sometimes.
SEE **AFTER SCRIPTS**).

### main

Consists in the father function of the proccess. Its responsible for
forward the choose menu, and start the choosen procedures.

The function verifies if the user has administrative permissions, assuring
that all the configuration files and programs be installed, without user
intervention.

The program integrity is verified through the **test_hash** funtion. If no
problems are found, continues normally. If a hash is not confirmed, the
program exits.

The program has **$WORKDIR** as master variable, that contains the path
were the program runs from. There are no problems in running the program
from another directory, as long the default structure be kept. Any
additional changes are charged to system administrator.

The general context of presentetion os shown, and follows a connectivity
test. As the UTFPR demands web login, this command should force the login
screen to appear. It follows a wait time for the login to be made.

If needed, the login might be done through the use if wget, or curl (common
case in UTFPR):

	curl -kd "username=ghost&password=666slapintheear"
https://intranet.pb.utfpr.edu.br/login

	wget --no-check-certificate
--post-data="username=ghost&password=666slapintheear"
https://intranet.pb.utfpr.edu.br/login

Remember to clean bash history, so the username is not kept

The macs are located in **$WORKDIR**/**$ROOM**/data/macs, with 200 as a
wildcard room, for not localized computers, and it is used to identify the
computer through the **PCID**. If successufuly identified, the functions
are now **customized** according to the **$ROOM**.

Lastly, It verifies, as well, if not called with an arguments, to specify
the procedure, or if was normally called, culminating on
**default_procedure** execution.

#### Without arguments - default procedure

The administrator may alters the **$PCID** according to its necessities, by
pressing any key in a **$TIME** specified period. If not interaction is
made, the proccess follows with the shown **$PCID**.  If The specified PCID
is considered invalid (non existing room, for example), a default format
will be taken, being prefixed by 200.

To alter **$PCID** implies the removal of the computer's macs from the
original list, and the generation of an amount of lines, equal to quantity
of macs, on the **$PCID**-specified mac list. This file might not be
ordered.

#### With arguments - Specifieds Steps

The **menu()** function is called with the given arguments, executating
them in specified order. If any argument is invalid, the program shows the
list if options, and exits.

### test_hash

Recieves no arguments. The funtion creates a tar file from **/spec** in
**tmp/verify.tar**, to generate its hash, and for doing so, takes in
account files created, erased or modified. Then it verifies if the
generated hash with the last healthy one, and if they are equal, updates
the old healthy tar with the new one (to ensure updates). If not, warns the
administrator, saving the generated tar and its log.

### update_hash

Forces the update of the hashs and security files, generating a new tar
over the last healthy, overwriting its hash and saving the log.

### id_mac

Recieves no arguments. Calls **look_in** for all and each directory
specified in **spec/**, one time for each machine mac address. Alocates the
**look_in** return value and evaluates it: If different from zero, returns
the room's number in which was found, concatenating to **look_in** result.
This configures the **$PCID** pattern. Otherwise, returns **$DEFAULT_DIR**
followed by the number of unindentified computers +1.  Unindentified
computers are are listed in **spec/$DEFAULT_DIR/data/macs**.

### look_in

Recieves the path of a text file, containing the mac list of a room, as
argument. Extracts the mac address and compares, in file lines order.  If
the machine macs are contained in the file, returns the machine number
according to the file. Otherwise, returns 0.

### room_of

Extracts the room's number from the **$PCID**

### id_of

Extracts the machine id from the **$PCID**

### change_id

Recieves, as argument, an **$PCID** pattern. Verifies if the machine has a
mac in the file specified by the old **$PCID** (not the argument),
according to **id_mac** answer, and if its confirmed, removes all according
macs from the file. Finally, forwards the new **$PCID** argument to
**add_mac** function.

### add_mac

May recieve an **$PCID** pattern. And if it does, validates the patterns
through **validate_pcid**. Independent of the validity, adds the macs pre
sent in **$macs** to the archive of the specified room.

### validate_pcid

Recieves a pattern **$PCID** as argument and validates. It's considered
valid the PCID which has the length of 5 characters (as example, 12345),
and the room, corresponding to the firsts three numbers, exists. If valid,
returns the PCID, if not, returns **$DEFAULT_DIR** followed by the id of
the invalid PCID (2 last digits) if invalidated by not exisistance of the
room, or 01, if invalidated by length.

### default_procedure

This is the master function which executes the default procedure of
configuration. Does not recieves any parameters. Actually, it's a wrapper.

#### Programmer's note: 

	**_customize_** a file stands for choosing a file, between one in
the specified room, or the default file. The specified file has priority
over the general. Given that this machine "/etc/example" file will be
**customized** as room's 002 defaults, this machine's file will be
overwritten by the 002 room's file, considering adaptations. If the 002
room's file cannot be found, **$DEFAULT_DIR** MUST have, as well, a version
of the file, in order to substitute the missing one. Overwritting a file by
the default room's file is understood as having a default configuration.

### user_config

The function first calls **find_impostors**. Then adds the default
**customized** users (**$LUSER** and **$AUSER**), and configure passwords
(**AUSER_P**, **LUSER_P**, and **ROOT_P**) by altering the current
/etc/shadow. The **AUSER** will be added to /etc/sudoers file. Alas, the
default behavior is the **$LUSER** to have no password.

### find_impostors

The function uses **$UID** to check whether a user was manually created, or
not, removing the ones which was (if **$UID** is lesser than 1000, it is
kept). If the user which wold be deleted is logged in, t's ignored.

### copy

	Recives a source and a destiny as parameters. If the source file
exists, it will copy to destination, overwriting any file.

### remove

	Recives a source as parameter. If the source file exists, it will
delete the file or directory.

### concatenate

	Recives a source, a destiny, and a overiting flag as parameters. If
the source file exists, and the overwite flag is not set, it will append
the source file to the end of the destiny file. If overwrite is set, the
destiny fil's content is ovewritten.

### uptodate

Checks for apt's lock-files, and if the are any, will warn the user about
the running proccesses, and exit. If no conflicting proccess were found,
updates the computer, according to **full-upgrade**

### build_lists

Generates the list of applications to be installed and removed from the
computer, by concatenating the **customized $WORKDIR/$ROOM/tmp/prog_ins**
file with the default one. The same proccess is made for **prog_rem**. The
removal list has priority over the installation list. In other words, if an
example1 program it's in the installation list, as well in the removal
list, he will not be installed.

Then, the apps in the **/tmp/prog_rem** list are removed using **apt -y
purge**, and then installs the apps in the **/tmp/prog_ins** list, using
**apt -y install**.

Finally, deletes the generated lists.

### package_config

It's a wrapper funtion. Calls the **customized** script **prec_ins.sh**,
responsible for instalating pre-compiled programs, the way each program
demands.

### incognito_conf

Makes the activation of **customized** incognito_conf script, found in
**$SCRIPT**. This script is installed by a job, **customized** as well, and
indicated by **$SERVICE**. Then redefines the user permissions to those
files as 450 and 440, respectively.

At last, enables the job through **systemctl**

Be aware of the concatanation, and not overwriting of this file

### grubify

**customizes** the script used by grub to generate configurations files,
and assures the change by generating a new configuration file through
**update-grub**.

### post-clear

Some applications are installed just for other apps support. This functions
removes those apps, **customized** in **prog_aft**, by using **apt -y
purge**

# BUGS

Send an email to smithcookiesmith<at>gmail.com
