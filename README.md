# sysconfig.sh - DAINF, UTFPR

## NOME

sysconfig.sh - Configura o sistema operacional, de acordo com padrões do bloco V

## SINOPSE

sysconfig.sh [_options_]

## DESCRIÇÃO

Sua premissa é facilitar o gerenciamento das máquinas do bloco V. Nós temos
muitos computadores, então a manutenção física de cada computador,
periodicamente, é impraticável. Este script automatiza as configurações,
exigindo apenas supervisão. Será, ainda, escrito um script para garantir
sucesso deste script.

O script, por si só, não exigirá interação humana além de
iniciá-lo. Nenhuma opção específica é necessária.

Problemas comuns:

Se um pacote licenciado (como Cisco PacketTracer e VirtualBox) for
instalado, será necessária aceitar uma licença. Estes programas exigem
intervenção do usuário.

A distribuição GNU/Linux pode ser qualquer derivada do Debain. Debian é
recomendado. Mas esteja ciente: no Debian, novas instalações precisam ter
"/etc/apt/sources.list" revisado, pois pode conter repositórios ativos
dependentes de discos.

Configurações como tabela de particionamento são irrelevantes, mas algumas
recomendações podem ser feitas:

Separar /boot (1GB). Evita falhas de inicialização quando os discos estão cheios.

Separar /var (>10GB). Alguns computadores Dell têm problemas com o manuseio do TPM
através do kernel linux e retorna muitos erros, o que aumenta os logs.
Encontre uma solução, ou limpe os logs de tempos em tempos.

Separar /home (<20GB). Você ficará impressionado com a capacidade dos alunos
em encher o disco com lixo.


## TAREFAS

O script é na verdade uma versão scriptada de tarefas comuns que um administrador do sistema
tem que fazer:

Configurar usuários

atualizar o computador

Identificá-lo de acordo

instalar aplicativos necessários (alguns deles compilados, alguns baixados, mas
a maioria dos repositórios)

Configurar SSH

Cada uma dessas tarefas foram separadas em funções, e podem ser chamadas
separadamente, mas o caso mais comum é executá-las todas.

## OPÇÕES

NOTA: Todas as opções marcadas por * são **customizados**

--info

Retorna informações sobre a identificação do computador

--test-hash

Verifique se o programa foi alterado

--update-hash

Atualizar alterações feitas no programa

--change-id

Alterar a identificação do PC para esta sessão

--change-id-permanent

Alterar a identificação do PC a cada execução

--ssh-config *

Instalar arquivos e chaves de configuração SSH

--package-conf *

Construir lista de programas e instalá-los

--incognito-conf *

Instale o serviço de navegação anônima

--hostname-conf *

Alterar o hostname

--grubify *

Instale os arquivos de configuração do grub e atualize-os

--find-impostors

Identificar e remover usuários não padrão

--user-conf

Instalar arquivos de configuração de usuários padrão

--help

Mostrar esta mensagem

## FUNÇÕES

As funções serão explicadas em ordem de evocação, como no
**default_procedure**, que segue sem intervenção do usuário (às vezes.
VER **APÓS SCRIPTS**).

### principal

Consiste na função paterna do processo. É responsável por
encaminhar o menu de escolhar, e iniciar os procedimentos escolhidos.

A função verifica se o usuário possui permissões administrativas, garantindo
que todos os arquivos de configuração e programas sejam instalados, sem
intervenção do usuário.

A integridade do programa é verificada através da função **test_hash**. Se
problemas não foram encontrados, continua normalmente. Se um hash não for confirmado, o
programa sai.

O programa tem **$WORKDIR** como variável mestre, que contém o caminho
de onde o programa é executado. Não há problemas na execução do programa
de outro diretório, desde que a estrutura padrão seja mantida. Quaisquer
mudanças adicionais ficam a cargo do administrador do sistema.

O contexto geral de apresentação é mostrado, e segue um teste de
conectividade. Como a UTFPR exige login, este comando deve forçar a tela de
login a aparecer. Segue-se um tempo de espera para que o login seja feito.

Se necessário, o login pode ser feito através do uso de wget, ou curl
(procedimento padrão na UTFPR):

curl -kd "username=ghost&password=666slapintheear" https://intranet.pb.utfpr.edu.br/login

wget --no-check-certificate --post-data="username=ghost&password=666slapintheear" https://intranet.pb.utfpr.edu.br/login

Lembre-se de limpar o histórico do bash, para que o nome de usuário não
seja salvo

Os macs estão localizados em **$WORKDIR**/**$ROOM**/data/macs, com 200 como
sala curinga para computadores não localizados, e é usado para identificar o
computador através do **PCID**. Se identificadas com sucesso, as funções
agora são **customiados** de acordo com o **$ROOM**.

Por último, verifica, também, se não for chamado com argumentos, para especificar
o procedimento, ou se era normalmente chamado, culminando na execução do
**default_procedure**.

#### Sem argumentos - procedimento padrão

O administrador pode alterar o **$PCID** conforme sua necessidade,
pressionando qualquer tecla em um período especificado **$TIME**. Se a
interação não for feita, o processo segue com o **$PCID** mostrado. Se o PCID especificado
é considerado inválido (sala inexistente, por exemplo), um formato padrão
será tomado, sendo prefixado por 200.

Alterar **$PCID** implica a remoção dos macs do computador da
lista original, e a geração de uma quantidade de linhas, igual a quantidade
de macs, na lista de macs especificada por **$PCID**. Este arquivo pode não
estar ordenado.

#### Com argumentos - Etapas especificadas

A função **menu()** é chamada com os argumentos fornecidos, executando
-os na ordem especificada. Se algum argumento for inválido, o programa mostra o
lista se opções e sai.

### test_hash

Não recebe argumentos. A função cria um arquivo tar de **/spec** em
**tmp/verify.tar**, para gerar seu hash e, para isso, leva em
conta arquivos criados, apagados ou modificados. Em seguida, verifica se o
gerou hash com o último saudável, e se forem iguais, atualiza
o antigo tar saudável com o novo (para garantir atualizações). Caso contrário, avisa o
administrador, salvando o tar gerado e seu log.

### update_hash

Força a atualização dos hashs e arquivos de segurança, gerando um novo tar
sobre o último saudável, sobrescrevendo seu hash e salvando o log.

### id_mac

Não recebe argumentos. Chama **look_in** para todos e cada diretório
especificado em **spec/**, uma vez para cada endereço mac da máquina. Aloca o
retorno de **look_in** e o avalia: Se diferente de zero, retorna
o número da sala em que foi encontrado, concatenado ao resultado **look_in**.
Isso define o padrão **$PCID**. Caso contrário, retorna **$DEFAULT_DIR**
seguido do número de computadores não identificados +1. Computadores não
identificados estão listados em **spec/$DEFAULT_DIR/data/macs**.

### look_in

Recebe o caminho de um arquivo texto, contendo a lista de macs de uma sala, como
argumento. Extrai o endereço mac e compara, na ordem das linhas do arquivo.
Se os macs da máquina estão contidos no arquivo, retorna o número da máquina
de acordo com o arquivo. Caso contrário, retorna 0.

### room_of

Extrai o número da sala de **$PCID**

### id_of

Extrai o ID da máquina de **$PCID**

### change_id

Recebe, como argumento, o padrão **$PCID**. Verifica se a máquina possui
mac no arquivo especificado pelo antigo **$PCID** (não o argumento),
de acordo com a resposta de **id_mac**, e se for confirmado, remove todos
os macs do arquivo que identificam este. Por fim, encaminha o novo
argumento **$PCID** para a função **add_mac**.

### add_mac

Pode receber um padrão **$PCID**. E se isso acontecer, valida os padrões
através de **validate_pcid**. Independente da validade, adiciona os macs
presentes em **$macs** para o arquivo da sala especificada.

### validate_pcid

Recebe um padrão **$PCID** como argumento e valida. É considerado
válido o PCID que tem o comprimento de 5 caracteres (por exemplo, 12345),
e o quarto, correspondente aos três primeiros números, existe. Se for válido,
retorna o PCID, caso contrário, retorna **$DEFAULT_DIR** seguido do id do PCID inválido (2 últimos dígitos) se invalidado pela não existência do
quarto, ou 01, se invalidado pelo comprimento.

### default_procedure

Esta é a função master que executa o procedimento padrão de
configuração. Não recebe nenhum parâmetro. Na verdade, é um wrapper.

#### Nota do programador:

**_customizar_** um arquivo significa escolher um arquivo, entre um na sala especificada ou o arquivo padrão. O arquivo especificado tem prioridade
sobre o geral. Dado que este arquivo "/etc/example" da máquina será
**customizada** como padrão da sala 002, o arquivo desta máquina será
sobrescrito pelo arquivo da sala 002, considerando as adaptações. Se o
arquivo da sala 002 não puder ser encontrado, **$DEFAULT_DIR** DEVE ter, também, uma versão
do arquivo, para substituir o que falta. Substituindo um arquivo por
o arquivo da sala padrão é entendido como tendo uma configuração padrão.

### user_config

A função primeiro chama **find_impostors**. Em seguida, adiciona o padrão
**customizado** de usuários (**$LUSER** e **$AUSER**) e configura senhas
(**AUSER_P**, **LUSER_P** e **ROOT_P**) alterando o valor atual de
/etc/shadow. O **AUSER** será adicionado ao arquivo /etc/sudoers. Além
disso, o comportamento padrão do **$LUSER** é não ter senha.

### find_impostors

A função usa **$UID** para verificar se um usuário foi criado manualmente ou
não, removendo os que foram (se **$UID** for menor que 1000, é
mantido). Se o usuário que seria excluído estiver logado, este é ignorado.

### copy

Recebe uma origem e um destino como parâmetros. Se o arquivo de origem
existe, irá copiar para o destino, sobrescrevendo qualquer arquivo.

### remove

Recebe uma origem como parâmetro. Se o arquivo de origem existir, exclui o arquivo ou diretório.

### concatenate

Recebe uma origem, um destino e um sinalizador de sobrescrita como parâmetros. Se
o arquivo de origem existe, e o sinalizador de sobrescrita não está
definido, o arquivo de origem será anexado ao final do arquivo de destino.
Se a sobreposição estiver definida, o
o conteúdo do arquivo de destino é substituído.

### uptodate

Verifica pelos arquivos de trava do apt, e se houver algum, alerta o
usuário sobre os processos que rodam, e sai. Se nenhum processo conflitante
for encontrado, autaliza o computador, de acordo com **full-upgrade**;

### build_lists

Gera a lista de aplicações a serem instaladas e removidas do computador,
através da concatenação do **$WORKDIR/$ROOM/tmp/prog_ins customizado** ao
padrão. O mesmo processo é feito para **prog_rem**. A lista de remoção tem
prioridade sobre a lista de instalação. Em outras palavras, se um programa
"exemplo1" está na lista de instalação e tambpem na de remoção, ele não
será instalado.

Então os aplicativos da lista em **/tmp/prog_rem** são removidos usando
**apt -y purge**, e então instala os aplicativos de **/tmp/prog_ins**,
usando **apt -y install**.

Por fim, deleta as listas geradas.

### package_config

É um wrapper. Chama o script **customizado** **perc_ins.sh**. responsável
por instalar programas pré-compilados, da forma que o programa precisar.

### incognito_conf

Faz a ativação **customizada** do incognito_conf script, encontrado em
**$SCRIPT**. Este script é instalado como um serviço, também
**customizado**, e indicado por **$SERVICE**. Então redefine as permissões
dos usuários sobre estes arquivos como 450 e 440, respectivamente.

Por fim, habilita o trabalho pelo **systemctl**.

Cuidado com a concatenação. Não sobreescreva este arquivo.

### grubify

**customiza** o script usado pelo grub para gerar os arquivos de
configuração, e garante a mudança gerando novos arquivos através do
**update-grub**.

### post-clear

Algumas aplicações são instaladas apenas apra suporte a outros aplicativos.
Essa função remove esses aplicativos, **customizado** em **prog_aft**,
usando **apt -y purge**.

# BUGS

Envie um email para smithcookiesmith<at>gmail.com
